package com.godt

import android.app.Application
import com.godt.domain.injection.*
import com.godt.presentation.recipes.RecipesContract

class App : Application() {

    val component: AppCompontent by lazy {
        DaggerAppCompontent.builder().appModule(AppModule(this)).build()
    }

    private var recipesSubcomponent: RecipesComponent? = null

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }

    fun getRecipesSubcomponent(view: RecipesContract.View): RecipesComponent? {
        if(recipesSubcomponent == null) recipesSubcomponent = component.plus(RecipesModule())
        return recipesSubcomponent
    }

    fun releaseRecipesSubcomponent() {
        this.recipesSubcomponent = null
    }


}