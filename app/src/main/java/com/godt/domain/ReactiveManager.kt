package com.godt.domain

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ReactiveManager {

    val schedulerAndroidMain = AndroidSchedulers.mainThread()

    val schedulerIo = Schedulers.io()

    val schedulerComputation = Schedulers.computation()

    val schedulerNewThread = Schedulers.newThread()
}