package com.godt.domain.injection

import javax.inject.Qualifier
import javax.inject.Scope

object Annotations {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME) annotation class ApiInfo

    @Scope
    @Retention(AnnotationRetention.RUNTIME) annotation class RecipeScope

}