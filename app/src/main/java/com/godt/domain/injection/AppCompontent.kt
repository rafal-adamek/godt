package com.godt.domain.injection

import com.godt.App
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class))
@Singleton
interface AppCompontent {
    fun inject(app: App)
    fun plus(module: RecipesModule): RecipesComponent
}