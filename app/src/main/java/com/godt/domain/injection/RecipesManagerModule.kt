package com.godt.domain.injection

import android.content.Context
import com.godt.data.RecipesDataStore
import com.godt.data.RecipesRepository
import com.godt.data.RecipesRepositoryImpl
import com.godt.data.local.LocalDataStore
import com.godt.data.models.Models
import com.godt.data.remote.GodtApi
import com.godt.data.remote.RemoteDataStore
import dagger.Module
import dagger.Provides
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import javax.inject.Singleton

@Module
class RecipesManagerModule {

    @Provides
    @Annotations.RecipeScope
    fun provideRecipesLocalDataStore(database: KotlinReactiveEntityStore<Persistable>): RecipesDataStore.Local
            = LocalDataStore(database)

    @Provides
    @Annotations.RecipeScope
    fun provideNotesRemoteDataStore(api: GodtApi): RecipesDataStore.Remote = RemoteDataStore(api)

    @Provides
    @Annotations.RecipeScope
    fun provideNotesRepository(localDataStore: RecipesDataStore.Local, remoteDataStore: RecipesDataStore.Remote): RecipesRepository =
            RecipesRepositoryImpl(localDataStore, remoteDataStore)


    @Provides
    @Annotations.RecipeScope
    fun provideDatabase(appContext: Context): KotlinReactiveEntityStore<Persistable> {
        val source = DatabaseSource(appContext, Models.DEFAULT, 1)
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        return KotlinReactiveEntityStore(KotlinEntityDataStore(source.configuration))
    }
}