package com.godt.domain.injection

import android.content.Context
import com.godt.App
import com.godt.domain.ReactiveManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(val app: App) {
    @Provides fun provideApp(): App = this.app

    @Provides fun provideAppContext(): Context = this.app.applicationContext

    @Provides @Singleton fun provideReactiveManager() = ReactiveManager()
}