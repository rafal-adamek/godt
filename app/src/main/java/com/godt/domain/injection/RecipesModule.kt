package com.godt.domain.injection

import com.godt.data.RecipesRepository
import com.godt.domain.ReactiveManager
import com.godt.presentation.recipes.RecipesContract
import com.godt.presentation.recipes.RecipesPresenter
import dagger.Module
import dagger.Provides

@Module
class RecipesModule {

    @Provides
    @Annotations.RecipeScope
    fun providePresenter(reactiveManager: ReactiveManager, recipesRepository: RecipesRepository): RecipesContract.Presenter
            = RecipesPresenter(reactiveManager, recipesRepository)
}