package com.godt.domain.injection

import com.godt.data.remote.GodtApi
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Annotations.ApiInfo
    @Annotations.RecipeScope
    fun provideBaseUrl() = GodtApi.Paths.BASE_URL

    @Provides
    @Annotations.RecipeScope
    fun provideGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Annotations.RecipeScope
    fun provideRxJavaAdapter(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides
    @Annotations.RecipeScope
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    @Provides
    @Annotations.RecipeScope
    fun provideOkhttpClient(): OkHttpClient =
            OkHttpClient.Builder().build()

    @Provides
    @Annotations.RecipeScope
    fun provideRetrofit(
            @Annotations.ApiInfo url: String,
            gson: Gson,
            okHttpClient: OkHttpClient,
            converterFactory: GsonConverterFactory,
            rxJavaCallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Annotations.RecipeScope
    fun provideApi(retrofit: Retrofit) = retrofit.create(GodtApi::class.java)

}