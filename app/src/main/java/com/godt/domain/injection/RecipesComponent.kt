package com.godt.domain.injection

import com.godt.presentation.recipes.RecipesActivity
import dagger.Subcomponent

@Annotations.RecipeScope
@Subcomponent(modules = arrayOf(RecipesModule::class, RecipesManagerModule::class, NetworkModule::class))
interface RecipesComponent {
    fun inject(activity: RecipesActivity)
}