package com.godt.presentation.main

class BaseContract {
    interface View
    interface FragmentView : View
    interface ActivityView : View
    interface Presenter {
        fun onDestroy()
    }
}