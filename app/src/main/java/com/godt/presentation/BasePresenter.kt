package com.godt.presentation

import com.godt.domain.ReactiveManager
import com.godt.presentation.main.BaseContract
import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<V : BaseContract.View>(
        protected val reactiveManager: ReactiveManager
) : BaseContract.Presenter {

    lateinit var view: V

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        this.compositeDisposable.dispose()
    }
}