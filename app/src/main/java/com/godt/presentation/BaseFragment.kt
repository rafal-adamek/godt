package com.godt.presentation

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.godt.App
import com.godt.presentation.main.BaseContract
import javax.inject.Inject

abstract class BaseFragment<V : ViewDataBinding, P : BaseContract.Presenter> : Fragment(), BaseContract.FragmentView {

    protected lateinit var root: V

    @Inject protected lateinit var presenter: P

    @get:LayoutRes protected abstract val layoutId: Int

    val app: App by lazy { activity.application as App }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return root.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        this.root.unbind()
        this.presenter.onDestroy()
        super.onDestroy()
    }
}