package com.godt.presentation

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.godt.App
import com.godt.presentation.main.BaseContract
import javax.inject.Inject

abstract class BaseActivity<V : ViewDataBinding, P : BaseContract.Presenter> : AppCompatActivity(), BaseContract.ActivityView {

    protected lateinit var root: V

    @Inject protected lateinit var presenter: P

    @get:LayoutRes protected abstract val layoutId: Int

    val app: App by lazy { application as App }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        root = DataBindingUtil.setContentView(this, layoutId)
    }

    fun replaceFragment(
            fragment: Fragment,
            tag: String? = null,
            @IdRes containerId: Int,
            addToBackStack: Boolean = true) {

        supportFragmentManager.beginTransaction().apply {
            if (addToBackStack) addToBackStack(null)
            else disallowAddToBackStack()
            replace(containerId, fragment, tag)
        }.commit()
    }

    fun getFragment(fragmentTag: String): Fragment? = supportFragmentManager.findFragmentByTag(fragmentTag)

    override fun onDestroy() {
        this.root.unbind()
        this.presenter.onDestroy()
        super.onDestroy()
    }

}