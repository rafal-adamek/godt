package com.godt.presentation.recipes

import com.godt.data.models.Recipe
import com.godt.presentation.main.BaseContract

class RecipesContract {
    interface Presenter : BaseContract.Presenter {
        fun loadRecipes()
        fun provideRecipeList(): List<Recipe>
        fun init()
        fun attachView(v: View)
        fun filter(filter: String): List<Recipe>
    }
    interface View : BaseContract.View {
        fun init()
        fun showProgress(isShowing: Boolean)
        fun showError()
        fun notifyRecipeAdded(position: Int)
    }
}