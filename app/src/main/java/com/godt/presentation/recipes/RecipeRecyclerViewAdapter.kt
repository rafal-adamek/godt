package com.godt.presentation.recipes

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.godt.BR
import com.godt.data.models.Recipe
import com.godt.databinding.ItemRecipeBinding

class RecipeRecyclerViewAdapter(
        private var values: List<Recipe>
) : RecyclerView.Adapter<RecipeRecyclerViewAdapter.RecipeHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemRecipeBinding.inflate(inflater, parent, false)
        return RecipeHolder(binding)
    }

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        val recipe = values[position]
        holder.root.setVariable(BR.recipe, recipe)
        holder.root.executePendingBindings()
        Glide.with(holder.root.root)
                .load(recipe.images[0].url)
                .into(holder.root.ivImage)
        holder.root.tvIngredientsContent.text = recipe.ingredients.map { it.name }.toString()
    }

    fun replaceAll(recipes: List<Recipe>) {
        this.values = recipes
    }

    override fun getItemCount(): Int = this.values.size

    inner class RecipeHolder(val root: ItemRecipeBinding) : RecyclerView.ViewHolder(root.root)
}