package com.godt.presentation.recipes

import android.content.res.Configuration
import android.os.Bundle
import android.widget.SearchView
import android.widget.Toast
import com.godt.R
import com.godt.databinding.ActivityRecipeBinding
import com.godt.presentation.BaseActivity

//TODO: Permissions

class RecipesActivity : BaseActivity<ActivityRecipeBinding, RecipesContract.Presenter>(), RecipesContract.View, SearchView.OnQueryTextListener {

    override val layoutId: Int = R.layout.activity_recipe

    private val recipeAdapter by lazy {
        RecipeRecyclerViewAdapter(this.presenter.provideRecipeList())
    }

    //Lifecycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.app.getRecipesSubcomponent(this)?.inject(this)
        this.presenter.attachView(this)
        this.root.contentMain.rvRecipes.adapter = recipeAdapter
        this.root.contentMain.srlRecipes.setOnRefreshListener { this.presenter.loadRecipes() }
        this.root.contentMain.svSearch.setOnQueryTextListener(this)
        this.presenter.init()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        this.recipeAdapter.notifyDataSetChanged()
    }

    //Implementation

    override fun init() {
        this.presenter.loadRecipes()
    }

    override fun showProgress(isShowing: Boolean) {
        this.root.contentMain.srlRecipes.isRefreshing = isShowing
    }

    override fun notifyRecipeAdded(position: Int) {
        this.recipeAdapter.notifyItemInserted(position)
    }

    override fun showError() {
        Toast.makeText(this, resources.getString(R.string.error_connection), Toast.LENGTH_SHORT).show()
    }

    override fun onQueryTextSubmit(p0: String): Boolean = false


    override fun onQueryTextChange(p0: String): Boolean {
        val filtered = presenter.filter(p0)
        this.recipeAdapter.replaceAll(if (p0.isEmpty()) this.presenter.provideRecipeList() else filtered)
        this.recipeAdapter.notifyDataSetChanged()
        return filtered.isEmpty()
    }
}