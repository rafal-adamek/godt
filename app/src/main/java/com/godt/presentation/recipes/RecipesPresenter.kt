package com.godt.presentation.recipes

import com.godt.data.RecipesRepository
import com.godt.data.models.Recipe
import com.godt.domain.ReactiveManager
import com.godt.presentation.BasePresenter
import com.godt.presentation.utils.LogUtil
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class RecipesPresenter @Inject constructor(
        reactiveManager: ReactiveManager,
        private val repository: RecipesRepository
) : BasePresenter<RecipesContract.View>(reactiveManager), RecipesContract.Presenter {

    private val recipes = mutableListOf<Recipe>()

    override fun attachView(v: RecipesContract.View) {
        this.view = v
    }

    override fun init() {
        if (this.recipes.isEmpty()) this.view.init()
    }

    override fun loadRecipes() {
        this.view.showProgress(true)
        this.repository.getRecipes()
                .observeOn(reactiveManager.schedulerAndroidMain)
                .subscribeOn(reactiveManager.schedulerIo)
                .subscribe(
                        { recipe: Recipe ->
                            LogUtil.d(RecipesPresenter.TAG, "Fetched recipe: $recipe")
                            if (!recipes.contains(recipe)) recipes.add(recipe)
                            view.notifyRecipeAdded(recipes.size)
                        },
                        { throwable: Throwable? ->
                            LogUtil.d(RecipesPresenter.TAG, "Error: $throwable")
                            this.view.showError()
                            this.view.showProgress(false)
                        },
                        {
                            LogUtil.d(RecipesPresenter.TAG, "Fetching complete. Current recipes size is: ${recipes.size}")
                            view.showProgress(false)
                        },
                        { disposable: Disposable ->
                            compositeDisposable.add(disposable)
                        }
                )
    }

    override fun provideRecipeList(): List<Recipe> = this.recipes

    override fun filter(filter: String): List<Recipe> =
            recipes.filter {
                        it.title.toLowerCase().contains(filter.toLowerCase())
                        || it.ingredients.map { it.name.toLowerCase() }.contains(filter.toLowerCase())
            }

    companion object {
        val TAG = RecipesPresenter::javaClass.toString()
    }

}