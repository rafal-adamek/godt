package com.godt.data

import com.godt.data.models.Recipe
import io.reactivex.Observable
import io.reactivex.Single

interface RecipesDataStore {
    fun getRecipes(): Observable<Recipe>

    interface Local : RecipesDataStore {
        fun saveRecipe(recipe: Recipe): Single<Recipe>
    }

    interface Remote : RecipesDataStore

}