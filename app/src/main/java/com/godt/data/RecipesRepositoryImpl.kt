package com.godt.data

import com.godt.data.models.Recipe
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class RecipesRepositoryImpl @Inject constructor(
        private val localDataStore: RecipesDataStore.Local,
        private val remoteDataStore: RecipesDataStore.Remote
) : RecipesRepository{

    override fun getRecipes(): Observable<Recipe> = remoteDataStore
            .getRecipes()
            .onErrorResumeNext { throwable: Throwable ->
                return@onErrorResumeNext localDataStore.getRecipes()
            }
            .doOnNext {
                this.saveRecipe(it).subscribe()
            }

    override fun saveRecipe(recipe: Recipe): Single<Recipe> = localDataStore.saveRecipe(recipe)

}