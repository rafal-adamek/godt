package com.godt.data

import com.godt.data.models.Recipe
import io.reactivex.Observable
import io.reactivex.Single

interface RecipesRepository {

    fun getRecipes(): Observable<Recipe>

    fun saveRecipe(recipe: Recipe): Single<Recipe>

}