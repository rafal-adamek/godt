package com.godt.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Image(
        @SerializedName("imboId") @get:Key var id: String,
        @SerializedName("url") var url: String
) : Persistable