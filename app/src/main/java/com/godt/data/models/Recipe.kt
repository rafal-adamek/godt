package com.godt.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Recipe(
        @SerializedName("id") @get:Key var id: Int,
        @SerializedName("title") var title: String,
        @SerializedName("description") var description: String,
        @SerializedName("ingredients") var ingredients: List<Ingredient>,
        @SerializedName("images") var images: List<Image>
) : Persistable