package com.godt.data.models

import com.google.gson.annotations.SerializedName
import io.requery.Entity
import io.requery.Key
import io.requery.Persistable

@Entity
data class Ingredient(
        @SerializedName("id") @get:Key var id: String,
        @SerializedName("name") var name: String
) : Persistable