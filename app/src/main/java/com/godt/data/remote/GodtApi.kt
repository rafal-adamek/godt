package com.godt.data.remote

import com.godt.data.models.Recipe
import io.reactivex.Observable
import retrofit2.http.GET

interface GodtApi {

    /**
     * Fetch recipes from URL. No parameters
     */
    @GET(Paths.RECIPES)
    fun fetchRecipes(): Observable<List<Recipe>>

    object Paths {
        internal const val BASE_URL = "https://www.godt.no/api/"
        internal const val RECIPES = "getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1&limit=50&from=0"
    }
}