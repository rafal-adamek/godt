package com.godt.data.remote

import com.godt.data.RecipesDataStore
import com.godt.data.models.Recipe
import io.reactivex.Observable
import javax.inject.Inject

class RemoteDataStore @Inject constructor(
        private val api: GodtApi
): RecipesDataStore.Remote {
    override fun getRecipes(): Observable<Recipe> = api
            .fetchRecipes()
            .flatMapIterable { x -> x }
}