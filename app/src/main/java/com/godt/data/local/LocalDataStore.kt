package com.godt.data.local

import com.godt.data.RecipesDataStore
import com.godt.data.models.Recipe
import io.reactivex.Observable
import io.reactivex.Single
import io.requery.Persistable
import io.requery.reactivex.KotlinReactiveEntityStore
import javax.inject.Inject

class LocalDataStore @Inject constructor(
        private val database: KotlinReactiveEntityStore<Persistable>
) : RecipesDataStore.Local {

    override fun getRecipes(): Observable<Recipe> = database
            .select(Recipe::class)
            .get()
            .observable()

    override fun saveRecipe(recipe: Recipe): Single<Recipe> = database.upsert(recipe)
}