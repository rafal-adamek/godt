package com.godt.presentation

import com.godt.domain.ReactiveManager
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
abstract class PresenterTest {

    @Mock lateinit var reactiveManagerMock: ReactiveManager

    protected val testScheduler = TestScheduler()

    @Before
    open fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(reactiveManagerMock.schedulerAndroidMain).thenReturn(testScheduler)
        Mockito.`when`(reactiveManagerMock.schedulerIo).thenReturn(testScheduler)
    }
}