package com.godt.presentation.recipes

import com.godt.data.RecipesRepository
import com.godt.data.models.Recipe
import com.godt.presentation.PresenterTest
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito

//TODO: Add tests

class RecipesPresenterTest : PresenterTest() {

    private lateinit var presenter: RecipesContract.Presenter

    @Mock lateinit var recipeViewMock: RecipesContract.View
    @Mock lateinit var repositoryMock: RecipesRepository
    @Mock lateinit var recipeMock: Recipe

    @Before
    override fun setup() {
        super.setup()
        this.presenter = RecipesPresenter(reactiveManagerMock, repositoryMock)
        this.presenter.attachView(recipeViewMock)
        Mockito.`when`(repositoryMock.getRecipes()).thenReturn(Observable.just(recipeMock))
    }

    @Test
    fun shouldShowProgressOnLoadRecipes() {
        this.presenter.loadRecipes()
        testScheduler.triggerActions()
        Mockito.verify(recipeViewMock).showProgress(ArgumentMatchers.eq(true))
    }

}